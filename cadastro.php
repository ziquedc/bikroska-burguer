<?php include('cabecalho.php') ?>


    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
            <h2 style="color:#f55a3b">Cadastro </h2>
                <div class="col-md-4">
                    
                    <form id="form">
                        <div class="form-group">
                            <div class="mdl-textfield mdl-js-textfield">
                                <input class="mdl-textfield__input" type="text" id="nome" name="nome" required>
                                <label class="mdl-textfield__label" >Nome Completo</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mdl-textfield mdl-js-textfield">
                                <input class="mdl-textfield__input" type="text" id="cpf" name="cpf" required>
                                <label class="mdl-textfield__label" >CPF</label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="mdl-textfield mdl-js-textfield">
                                <input class="mdl-textfield__input" type="date" id="nascimento" name="nascimento" required>
                                <label class="mdl-textfield__label" >Data Nascimento</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mdl-textfield mdl-js-textfield">
                                <input class="mdl-textfield__input" type="text" id="telefone" name="telefone" required>
                                <label class="mdl-textfield__label" >Telefone</label>
                            </div>
                        </div>
                        
                                            
                    
                </div>
                    
                <div class="col-md-4"> 
                        <div class="form-group">
                            <div class="mdl-textfield mdl-js-textfield">
                                <input class="mdl-textfield__input" type="email" id="" name="email" required>
                                <label class="mdl-textfield__label" >E-mail</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mdl-textfield mdl-js-textfield">
                                <input class="mdl-textfield__input" type="password" id="senha" name="senha" required>
                                <label class="mdl-textfield__label" >Senha</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mdl-textfield mdl-js-textfield">
                                <input class="mdl-textfield__input" type="password" id="confirmasenha" name="confirmasenha" required>
                                <label class="mdl-textfield__label" >Confirmar Senha</label>
                            </div>
                        </div>

                        
                </div>

                <div class="col-md-4"> 
                    <div class="form-group">
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="text" id="cep" name="cep" required>
                            <label class="mdl-textfield__label" >CEP</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="text" id="cidade" name="cidade" required>
                            <label class="mdl-textfield__label" >Cidade</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="text" id="uf" name="uf" required>
                            <label class="mdl-textfield__label" >UF</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="text" id="rua" name="rua" required>
                            <label class="mdl-textfield__label" >Rua</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="text" id="bairro" name="bairro" required>
                            <label class="mdl-textfield__label" >Bairro</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="number" id="numero" name="numero" required>
                            <label class="mdl-textfield__label" >Número</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="text" id="complemento" name="complemento" >
                            <label class="mdl-textfield__label" >Complemento</label>
                        </div>
                    </div>
                <!-- Button Submit -->
                    <div class="product-details">
                        <div class="add-to-cart">
                            <button type="button" id="botao" class="add-to-cart-btn"><i class="fa fa-check"></i>  Finalizar Cadastro</button>
                        </div>
                    </div>
                <!-- /Button Submit -->
                </form>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
		<!-- /SECTION -->

		
    <script>
         $('#').submit(function(e){
            e.preventDefault();
            var form = $(this).serialize();

            $.ajax({
                type:'POST',
                data:form,
                url: 'salvar_cliente.php'
            }).done(function(data){
                console.log(data);
            }).fail(function({
                alert("Erro interno. Tente mais tarde");
            })
        });

        $('#botao').click(function(e){
            alert('ok');
        })


    
    </script>

		
<?php include('rodape.php') ?>