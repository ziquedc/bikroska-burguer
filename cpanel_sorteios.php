<?php include('cpanel_cabecalho.php') ?>

<main class="mdl-layout__content mdl-color--grey-100" style="margin-bottom: 50px">
   <div class="mdl-grid demo-content">
      <!--FORMULÁRIO DE CADASTRO DE PRODUTOS-->
      <div class="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col col-md-12" >
         <div class="container">
            <p class="h3 text-secondary" style="margin: 15px"> Sorteios</p>
            <hr>
            <div class="row">
               <div class="col-sm-6">
                  <form id="form">
                     <div class="form-row align-items-center">
                        <div class="col-auto">
                           <b>Produto:</b>
                           <input type="text" class="form-control mb-2" name="produto" required>
                        </div>
                        <div class="col-auto">
                           <b>Ganhador:</b>
                           <input type="text" class="form-control mb-2" name="ganhador" required>
                        </div>
                     </div>
                     <div class="col-auto">
                        <button type="submit" class="btn btn-primary mb-2">Salvar</button>
                     </div>
                     <div id="msg" class="alert " role="alert" style="display: none;"></div>
               </div>
               </form>
               <div class="col-sm-6">
                  <form id="form2">
                     <div class="form-row align-items-center">
                        <div class="col-sm">
                           <b>Título do Sorteio:</b>
                           <input type="text" class="form-control mb-2" name="titulo" required>
                        </div>
                        <div class="col-auto">
                           <button type="submit" class="btn btn-primary mt-2">Salvar</button>
                        </div>
                        <div id="msg" class="alert " role="alert" style="display: none;"></div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="container">
           <hr>
            <div class="row">
               <div class="col-sm-12" style="margin-top: 30px">
                  <p class="h4 text-secondary" style="margin: 15px"> Ganhadores</p>
                  <!--TABELA DE LISTAGEM DE PRODUTOS-->
                  <table class="table table-striped ">
                     <thead class="">
                        <tr>
                           <th scope="col">Produto</th>
                           <th scope="col">Ganhador</th>
                           <th scope="col">Ações</th>
                        </tr>
                     </thead>
                     <tbody id="tabela">
                     </tbody>
                  </table>
               </div>
               
            </div>
         </div>
         <hr>
         <div class="container">
               <div class="col-sm-12" style="margin-top: 30px">
                <p class="h4 text-secondary" style="margin: 15px"> Título Sorteio</p>
                  
                  <!--TABELA DE LISTAGEM DE PRODUTOS-->
                  <table class="table table-striped ">
                     <thead class="">
                        <tr>
                           <th scope="col">Título</th>
                           <th scope="col">Ações</th>
                        </tr>
                     </thead>
                     <tbody id="tabela2">


                     </tbody>
                  </table>
               </div>
            </div>
         </div>



      </div>
   </div>
   </div>
</main>
<script>
   //preenche as tabelas ao 
   $(document).ready(function(){
     //popularTabela();
    // popularTabela2();
   });
   
   
   $('#form').submit(function(e){
     e.preventDefault();
     var form = $(this);
     salvarForm(form);
   })//fim submit
   
   //funcao chamada acima
   function salvarForm(dados){
      $.ajax({
       type:'POST',
       data:dados.serialize(),
       url:'salvar_sorteio.php'
     }).done(function(data){
       
       $sucesso = $.parseJSON(data)['sucesso'];
       $mensagem = $.parseJSON(data)['mensagem'];
       $erro = $.parseJSON(data)['mensagem'];
       console.log($erro);
       if($sucesso){
         $('#msg').removeClass('alert-danger');
         $('#msg').addClass('alert-success');
         $('#msg').show();
         $('#msg').html($mensagem);
         $('#produto').val(" ");
         $('#ganhador').val(" ");
         popularTabela();
         setTimeout(function(){ $('#msg').fadeOut() }, 4000);
   
       }else{
         $('#msg').removeClass('alert-success');
         $('#msg').addClass('alert-danger');
         $('#msg').show();
         $('#msg').html($mensagem);
   
         
       } 
   
     }).fail(function(){
       alert('Erro interno. Contate o administrador');
     });
   }
   
   
   
   //função para popular a tabela 
   function popularTabela(){
     $('#tabela').empty(); //limpa a tabela
     $.ajax({
       type:'POST',
       dataType:'json',
       url:'sorteios_json.php'
   
     }).done(function(data){
       for(var i =0; data.length>i; i++){
       
         //preenche a tabela
         $('#tabela').append("<tr><td> "+data[i].produto+" </td><td>  "+data[i].ganhador+"</td><td> <a id='excluir' href='' value='"+data[i].id_sorteio+"''><button class='btn'><i class='fa fa-trash fa-2x'></button></i></a> </td></tr>");
       }
   
      //evento de clique no botao de excluir
       $('#tabela tr td #excluir').click(function(e){
         e.preventDefault();
         var id = $(this).attr("value");
         var linha = $(this).parent().parent();
         exluirRegistro(id,linha);
       })  
   
   
     }).fail(function(){
       alert('erro interno. Contate o Administrador.')
   
     });
   }//fim função
   
   //FUNÇÃO DE EXCLUIR REGISTRO
   function exluirRegistro(cod, elemento){
     $.ajax({
       type:'POST',
       data:"id="+cod,
       url:'excluir_sorteio.php',
       asyn: true
     }).done(function(data){
       $sucesso = $.parseJSON(data)['sucesso'];
       $mensagem = $.parseJSON(data)['mensagem'];
   
       if($sucesso){
         elemento.fadeOut();
   
       }else{
         alert($mensagem);
       }
   
     }).fail(function(){
       alert('Erro interno. Contate o administrador');
     })
   }//fim função
   
   
   
   //FUNÇÃO PARA O TÍTULO
   $('#form2').submit(function(e){
     e.preventDefault();
     var form2 = $(this).serialize();
     salvarForm2(form2);
   })
   
   function salvarForm2(dados){
      $.ajax({
       type:'POST',
       data:dados,
       url:'salvar_titulo.php'
     }).done(function(data){
       
       $sucesso = $.parseJSON(data)['sucesso'];
       $mensagem = $.parseJSON(data)['mensagem'];
       $erro = $.parseJSON(data)['mensagem'];
       console.log($erro);
       if($sucesso){
         $('#msg').removeClass('alert-danger');
         $('#msg').addClass('alert-success');
         $('#msg').show();
         $('#msg').html($mensagem);
         $('#titulo').val(" ");
         popularTabela2();
         setTimeout(function(){ $('#msg').fadeOut() }, 4000);
   
       }else{
         $('#msg').removeClass('alert-success');
         $('#msg').addClass('alert-danger');
         $('#msg').show();
         $('#msg').html($mensagem);
   
         
       } 
   
     }).fail(function(){
       alert('Erro interno. Contate o administrador');
     });
   }


   //função para popular a tabela 
   function popularTabela2(){
     $('#tabela2').empty(); //limpa a tabela
     $.ajax({
       type:'POST',
       dataType:'json',
       url:'titulo_json.php'
   
     }).done(function(data){
       for(var i =0; data.length>i; i++){
       
         //preenche a tabela
         $('#tabela2').append("<tr><td> "+data[i].titulo+" </td><td> <a id='excluir2' href='' value='"+data[i].id_titulo+"''><button class='btn'><i class='fa fa-trash fa-2x'></button></i></a> </td></tr>");
       }
   
      //evento de clique no botao de excluir
       $('#tabela2 tr td #excluir2').click(function(e){
         e.preventDefault();
         var id = $(this).attr("value");
         var linha = $(this).parent().parent();
         exluirRegistro2(id,linha);
       })  
   
   
     }).fail(function(){
       alert('erro interno. Contate o Administrador.')
   
     });
   }//fim função
   
   //FUNÇÃO DE EXCLUIR REGISTRO
   function exluirRegistro2(cod, elemento){
     $.ajax({
       type:'POST',
       data:"id="+cod,
       url:'excluir_titulo.php',
       asyn: true
     }).done(function(data){
       $sucesso = $.parseJSON(data)['sucesso'];
       $mensagem = $.parseJSON(data)['mensagem'];
   
       if($sucesso){
         elemento.fadeOut();
   
       }else{
         alert($mensagem);
       }
   
     }).fail(function(){
       alert('Erro interno. Contate o administrador');
     })
   }//fim função



</script>





</div>
<!-- jQuery Plugins -->
<script src="_js/jquery.min.js"></script>
<script src="_js/bootstrap.min.js"></script>
<script src="_js/slick.min.js"></script>
<script src="_js/nouislider.min.js"></script>
<script src="_js/jquery.zoom.min.js"></script>
<script src="_js/main.js"></script>
</body>
</html>