<?php 
include_once('conexao.php');
$conn = getConncection();
$stm = $conn->prepare("SELECT * FROM produtos, categorias WHERE produtos.id_categoria = categorias.id_categoria AND promocao = 1");
$stm->execute();

//select nas categorias
$stm2 = $conn->prepare("SELECT * FROM categorias ORDER BY nome_categoria ASC");
$stm2->execute();


?>

<?php include('cabecalho.php') ?>

	

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- ASIDE -->
					<div id="aside" class="col-md-3">
						<!-- aside Widget -->
						<div class="aside">
							<h3 class="aside-title">Categorias</h3>
							<?php while($linha = $stm2->fetch(PDO::FETCH_ASSOC)){?>
							<div class="checkbox-filter">
								<div class="input-checkbox">
									<a href=""><label for="category-1">
										<span></span>
										<?php echo $linha['nome_categoria'] ?>
									</label></a>
								</div>
							</div>
							<?php }?>
						</div>
						<!-- /aside Widget -->


						

						
					</div>
					<!-- /ASIDE -->

					<!-- STORE -->
					<div id="store" class="col-md-9">
						

						<!-- store products -->
						<div class="row">

							<?php while($linha = $stm->fetch(PDO::FETCH_ASSOC)){
								$preco_normal = str_replace('.',',', $linha['preco_normal']);
								$preco_promo = str_replace('.',',', $linha['preco_promocional']);
							?>
							<!-- product -->
							<div class="col-md-4 col-xs-6">
								<div class="product">
								<form action="produto.php" method="POST">
								<input type="number" name="id_produto" value="<?php echo $linha['id_produto'] ?>" hidden >
									<div class="product-img">
										<img src="<?php echo $linha['imagem'] ?>" alt="">
										<div class="product-label">
											
										</div>
									</div>
									<div class="product-body">
										<p class="product-category"><?php echo $linha['nome_categoria'] ?></p>
										<h3 class="product-name"><a href="#"><?php echo $linha['nome'] ?></a></h3>
										<h4 class="product-price">R$<?php echo $preco_promo; ?> <del class="product-old-price">R$<?php echo $preco_normal; ?></h4>
										
									</div>
									<div class="add-to-cart">
										<button type="submit" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Venha conferir</button>
										</form>
									</div>
								</div>
							</div>
							<!-- /product -->
						<?php }?>
							

						
					</div>
					<!-- /STORE -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

<?php include('rodape.php') ?>

<script>
			//insert NEWSLETTER
			$('#formNews').submit(function(e){
				e.preventDefault();
				var form = $(this);
				
				$.ajax({
					type:"POST",
					data:form.serialize(),
					url:'salvar_newsletter.php'

				}).done(function(data){
					$sucesso = $.parseJSON(data)['sucesso'];
                    $mensagem = $.parseJSON(data)['mensagem'];

                    if($sucesso){
                      alert($mensagem);
                      $('#email').val(" ");
                     
                    }else{
                    	alert($mensagem);
                    } 



				}).fail(function(){
					alert("Não foi possível cadastrar no momento. Tente mais tarde!");
				});
			})


		</script>