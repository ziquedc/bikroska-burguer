<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Cpanel</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="_css/estilo.css">

    <!--icons-->  
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!--scroll tabela--> 
    <link rel="stylesheet" type="text/css" href="_css/tabela_scroll.css">
    <script src="_js/jquery.min.js"></script>
   

  </head>
  <body style="background-color: #d0d2d6">
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header" >
      <header class="demo-header mdl-layout__header mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="h3" style="margin-left: 90%">
      </div>
      </header>

      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50" >
        <header class="demo-drawer-header">
          <img src="img/logo-biroska-small.png" class="demo-avatar">
          <div class="demo-avatar-dropdown">
            <span>Biroska Burguer</span>
            <!-- Right aligned menu below button -->
            <button id="demo-menu-lower-right"
            class="mdl-button mdl-js-button mdl-button--icon ml-5">
            <i class="material-icons">more_vert</i>
            </button>

            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
            for="demo-menu-lower-right">
            <li class="mdl-menu__item">Some Action</li>
            <li class="mdl-menu__item">Another Action</li>
            <li disabled class="mdl-menu__item">Disabled Action</li>
            <li class="mdl-menu__item">Yet Another Action</li>
            </ul>
          </div>
        </header>

        <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
        <a class="mdl-navigation__link" href="cpanel_sorteios.php"><i class="fa fa-ticket fa-2x mr-2" aria-hidden="true"></i>Pedidos</a>

            <a class="mdl-navigation__link" href="cpanel_produto.php"><i class="fa fa-shopping-basket fa-2x mr-2" aria-hidden="true"></i>Produtos</a>

             <a class="mdl-navigation__link" href="cpanel_banner.php"><i class="fa fa-picture-o fa-2x mr-2" aria-hidden="true" ></i>Banner</a>

            <a class="mdl-navigation__link" href="cpanel_categorias.php"><i class="fa fa-bullseye fa-2x mr-2" aria-hidden="true"></i>Categorias</a>

            <a class="mdl-navigation__link" href="cpanel_promocoes.php"><i class="fa fa-calendar fa-2x mr-2" aria-hidden="true"></i>Promoções</a>

            
           
            <div class="mdl-layout-spacer"></div>
            <a class="mdl-navigation__link btn" href=""><i class="fa fa-sign-out mr-3 fa-2x" aria-hidden="true"></i>Sair</a>

        </nav>

      </div>

