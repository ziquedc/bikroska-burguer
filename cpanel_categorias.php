<?php include('cpanel_cabecalho.php') ?>


       <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
          
          <!--FORMULÁRIO DE CADASTRO DE PRODUTOS-->
          <div class="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col col-md-12" >
               
              <div class="container">
                <div class="row">
                  <div class="col-sm-4">
                    <p class="h3 text-secondary" style="margin: 15px"> Cadastrar Categoria</p>
                    <hr>
                   <form id="form" style="margin-top: 30px">
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="inputState"><b>Categoria</b></label>
                          <input type="text" class="form-control" id="categoria" name="categoria" required>
                        </div>
                      </div>
                      
                      <button type="submit" class="btn btn-primary">Salvar</button>
                    </form>
                    <div id="msg" class="alert " role="alert" style="display: none;"></div>
                  </div>
                  <div class="col-sm">
                    
                  </div>
                  <div class="col-sm-6">
                    <p class="h3 text-secondary" style="margin-top:15px"> Categorias Casdastradas</p>
                    <hr>
                    <!--TABELA DE LISTAGEM DE PRODUTOS-->
             
                      <table class="table table-striped ">
                      <thead class="">
                        <tr>
                          <th scope="col">Nome</th>
                          <th scope="col">Ações</th>
                        </tr>
                      </thead>
                      <tbody id="tabela">
                        
                        
                      </tbody>
                    </table>
                  </div>
                  
                </div>
              </div>

              <script>
                //preenche as tabelas ao 
                $(document).ready(function(){
                  popularTabela();
                });


                $('#form').submit(function(e){
                  e.preventDefault();
                  var form = $(this).serialize();
                  salvarForm(form);
                })//fim submit

                //funcao chamada acima
                function salvarForm(dados){
                   $.ajax({
                    type:'POST',
                    data:dados,
                    url:'salvar_categoria.php',
                    asyn: true
                  }).done(function(data){
                    $sucesso = $.parseJSON(data)['sucesso'];
                    $mensagem = $.parseJSON(data)['mensagem'];

                    if($sucesso){
                      $('#msg').removeClass('alert-danger');
                      $('#msg').addClass('alert-success');
                      $('#msg').show();
                      $('#msg').html($mensagem);
                      $('#categoria').val(" ");
                      popularTabela();

                    }else{
                      $('#msg').removeClass('alert-success');
                      $('#msg').addClass('alert-danger');
                      $('#msg').show();
                      $('#msg').html($mensagem);

                      
                    }

                  }).fail(function(){
                    alert('Erro interno. Contate o administrador');
                  });
                }



                //função para popular a tabela 
                function popularTabela(){
                  $('#tabela').empty(); //limpa a tabela
                  $.ajax({
                    type:'GET',
                    dataType:'json',
                    url:'categorias_json.php'

                  }).done(function(data){
                    for(var i =0; data.length>i; i++){
                    
                    //preenche a tabela
                    $('#tabela').append('<tr><td>' +data[i].nome_categoria + '</td><td> <a id="excluir" href="" alt="'+data[i].id_categoria+'"><button class="btn"><i class="fa fa-trash" aria-hidden="true"></button></a></td></tr>');
                  }

                   //evento de clique no botao de excluir
                    $('#tabela tr td #excluir').click(function(e){
                      e.preventDefault();
                      var id = $(this).attr("alt");
                      var linha = $(this).parent().parent();
                      exluirRegistro(id,linha);
                    })  


                  }).fail(function(){
                    alert('erro interno. Contate o Administrador.')

                  });
                }//fim função

                //FUNÇÃO DE EXCLUIR REGISTRO
                function exluirRegistro(cod, elemento){
                  $.ajax({
                    type:'POST',
                    data:"id="+cod,
                    url:'excluir_categoria.php',
                    asyn: true
                  }).done(function(data){
                    $sucesso = $.parseJSON(data)['sucesso'];
                    $mensagem = $.parseJSON(data)['mensagem'];

                    if($sucesso){
                      elemento.fadeOut();

                    }else{
                      alert($mensagem);
                    }

                  }).fail(function(){
                    alert('Erro interno. Contate o administrador');
                  })
                }//fim função


              </script>
                


                

               
            </div>


          

          
        </div>
      </main>

    </div>
 <!-- jQuery Plugins -->
    <!-- jQuery Plugins -->
    
    <script src="_js/bootstrap.min.js"></script>
    <script src="_js/slick.min.js"></script>
    <script src="_js/nouislider.min.js"></script>
    <script src="_js/jquery.zoom.min.js"></script>
    <script src="_js/main.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  </body>
  </html>
  