<?php 
include_once('database/conexao.php');
$conn = getConncection();
$stm = $conn->prepare("SELECT * FROM produtos, categorias WHERE produtos.id_categoria = categorias.id_categoria AND promocao = 1");
$stm->execute();

//select no banner
$stm2 = $conn->prepare("SELECT * FROM banner");
$stm2->execute();
$d = $stm2->fetch(PDO::FETCH_ASSOC);
$banner = $d['caminho'];

//select data final da promocao para o contador regressivo
$stm3 = $conn->prepare("SELECT * FROM  promocao");
$stm3->execute();
$data_final = $stm3->fetch(PDO::FETCH_ASSOC);
$data_final = $data_final['data_final'];


?>
<?php include('cabecalho.php') ?>

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
						<!-- tab -->
						<div id="tab2" class="tab-pane active">
							<div class="products-slick" data-nav="#slick-nav-20">
								<div class="col-md-4 col-xs-6">
									<div class="shop">
										<div class="shop-img">
											<img src="./img/kimia-zarifi-9V2YVX_Cojg-unsplash.jpg" alt="">
										</div>
										<div class="shop-body">
											<h3>Hambúrguer</h3>
											<a href="#" class="cta-btn">Mais info <i class="fa fa-arrow-circle-right" width="600px" height="400px"></i></a>
										</div>
									</div>
								</div>
								<!-- /shop -->


								<!-- shop -->
								<div class="col-md-4 col-xs-6">
									<div class="shop">
										<div class="shop-img">
											<img src="./img/fritas.jpg" alt="">
										</div>
										<div class="shop-body">
											<h3>Batata Frita</h3>
											<a href="#" class="cta-btn">Mais info <i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
								</div>
								<!-- /shop -->
								<!-- shop -->
								<div class="col-md-4 col-xs-6">
									<div class="shop">
										<div class="shop-img">
											<img src="./img/alimento-churrasqueira-comida-86127.jpg" alt="">
										</div>
										<div class="shop-body">
											<h3>Grill</h3>
											<a href="#" class="cta-btn">Mais info <i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
								</div>
								<!-- /shop -->
								<!-- shop -->
								<div class="col-md-4 col-xs-6">
									<div class="shop">
										<div class="shop-img">
											<img src="./img/combo.jpg" alt="">
										</div>
										<div class="shop-body">
											<h3>Combos</h3>
											<a href="#" class="cta-btn">Mais info <i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
								</div>
								<!-- /shop -->
									<!-- shop -->
									<div class="col-md-4 col-xs-6">
										<div class="shop">
											<div class="shop-img">
												<img src="./img/bebidas.jpg" alt="">
											</div>
											<div class="shop-body">
												<h3>Bebidas</h3>
												<a href="#" class="cta-btn">Mais info <i class="fa fa-arrow-circle-right"></i></a>
											</div>
										</div>
									</div>
									<!-- /shop -->
							</div>
							<div id="slick-nav-20" class="products-slick-nav"></div>
						</div>
						<!-- /tab -->

				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">Produtos</h3>
							<div class="section-nav">
								<ul class="section-tab-nav tab-nav">
									<li class="active"><a data-toggle="tab" href="#tab1">Novidades</a></li>
									
								</ul>
							</div>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab1" class="tab-pane active">
									<div class="products-slick" data-nav="#slick-nav-1">
										<?php while($linha = $stm->fetch(PDO::FETCH_ASSOC)){
											$preco_normal = str_replace('.',',', $linha['preco_normal']);
											$preco_promo = str_replace('.',',', $linha['preco_promocional']);
											?>

											<!-- product -->
											<div class="product">
												<form action="produto.php" method="POST">
												<input type="number" name="id_produto" value="<?php echo $linha['id_produto'] ?>" hidden >
												<div class="product-img">
													<img src="<?php echo $linha['imagem']; ?>" alt="">
												</div>
												<div class="product-body">
													<p class="product-category"><?php echo $linha['nome_categoria']; ?></p>
													<h3 class="product-name"><a href="#"><?php echo $linha['nome']; ?></a></h3>
													<h4 class="product-price">R$<?php echo $preco_promo; ?> <del class="product-old-price">R$<?php echo $preco_normal; ?></del></h4>
												</div>
												<div class="add-to-cart">
													
														<button type="submit" class="add-to-cart-btn"><i class="fa fa-plus"></i> Mais detalhes</button>
													</form>	
												</div>
											</div>
											<!-- /product -->
										<?php } ?>
									</div>
									<div id="slick-nav-1" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->
		<style type="text/css">
			#hot-deal.section {
			  padding: 90px 0px;
			  margin: 30px 0px;
			  background-color: #E4E7ED;
			  background-image: url('<?php echo $banner ?>');
			  background-position: center;
			  background-repeat: no-repeat;
			}
		</style>

		<!-- HOT DEAL SECTION -->
		<div id="hot-deal" class="section" >
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="hot-deal">
							<h2 class="text-uppercase">Ofertas expiram em:</h2>
							<!-- CRIAR CONTADOR REGRESSIVO EM JS -->
							<ul class="hot-deal-countdown" id="clock">
								<li>
									<div>
										<h3>02</h3>
										<span>Days</span>
									</div>
								</li>
								<li>
									<div>
										<h3>10</h3>
										<span>Hours</span>
									</div>
								</li>
								<li>
									<div>
										<h3>34</h3>
										<span>Mins</span>
									</div>
								</li>
								<li>
									<div>
										<h3>60</h3>
										<span>Secs</span>
									</div>
								</li>
							</ul>
							
							<a class="primary-btn cta-btn" href="produtos.php">Ver todos os Produtos</a>
								
							<script type="text/javascript">
							  $('#clock').countdown('<?php echo $data_final; ?>', function(event) {
							  var $this = $(this).html(event.strftime(''
							    
							    + '<li><div><h3>%d</h3><span>Dias</span></div></li>'
							    + '<li><div><h3>%H</h3><span>Horas</span></div></li>'
							    + '<li><div><h3>%M</h3><span>Mins</span></div></li>'
							    + '<li><div><h3>%S</h3><span>Seg</span></div></li>'));
							});
							</script>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /HOT DEAL SECTION -->
		

		
		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			
			<div class="container">
				
				<div class="row">
					<div class="col-md-12">
						<div class="newsletter">
							<p>Assine a <strong>NEWSLETTER</strong></p>
							<form id="formNews">
								<input class="input" type="email" name="email" id="email" placeholder="Insira seu e-mail" required>
								<button type="submit" class="newsletter-btn"><i class="fa fa-envelope"></i> Assinar</button>
							</form>
							<ul class="newsletter-follow">
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	<!--	NEWSLETTER -->
		

		<?php include('rodape.php') ?>

		<script>
			//insert NEWSLETTER
			$('#formNews').submit(function(e){
				e.preventDefault();
				var form = $(this);
				
				$.ajax({
					type:"POST",
					data:form.serialize(),
					url:'salvar_newsletter.php'

				}).done(function(data){
					$sucesso = $.parseJSON(data)['sucesso'];
                    $mensagem = $.parseJSON(data)['mensagem'];

                    if($sucesso){
                      alert($mensagem);
                      $('#email').val(" ");
                     
                    }else{
                    	alert($mensagem);
                    } 



				}).fail(function(){
					alert("Não foi possível cadastrar no momento. Tente mais tarde!");
				});
			})


		</script>
