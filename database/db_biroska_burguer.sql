-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Set-2019 às 01:47
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_biroska_burguer`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banner`
--

CREATE TABLE `banner` (
  `id_banner` int(11) NOT NULL,
  `caminho` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `banner`
--

INSERT INTO `banner` (`id_banner`, `caminho`) VALUES
(20, 'img/banner/22260648cc2158b03bb883c61709ffe1.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `nome_categoria` varchar(255) NOT NULL,
  `imagem_categoria` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `nome_categoria`, `imagem_categoria`) VALUES
(29, 'Bebidas', 'img/categorias/ed2b1bcf32d3c29848e97568bb220a07.jpg'),
(30, 'Grill', 'img/categorias/d1102d266f401a40814889acc03f9e96.jpg'),
(31, 'Batata Frita', 'img/categorias/b4ca2d6c9616effa99bdba55cd30b4c8.jpg'),
(32, 'Combo', 'img/categorias/0e43b88a9be0e0e4c9c7442f6872b10b.jpg'),
(33, 'Hambúrguer', 'img/categorias/70494c95288d97aaca9e72d732912f9a.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nome_cliente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpf_cliente` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `nascimento_cliente` date NOT NULL,
  `telefone_cliente` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email_cliente` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `senha_cliente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep_cliente` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_cliente` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `uf_cliente` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `rua_cliente` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `bairro_cliente` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `numero_cliente` int(10) NOT NULL,
  `complemento_cliente` varchar(35) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nome_cliente`, `cpf_cliente`, `nascimento_cliente`, `telefone_cliente`, `email_cliente`, `senha_cliente`, `cep_cliente`, `cidade_cliente`, `uf_cliente`, `rua_cliente`, `bairro_cliente`, `numero_cliente`, `complemento_cliente`) VALUES
(1, 'LUIZ HENRIQUE CAMPOS', '037-105-92', '2019-02-02', '5599714-8467', 'ZIQUEDC@GMAIL.COM', '123456', '98130-000', 'JULIO DE CASTILHOS', 'RS', 'AVENIDA FERNANDO ABOTT', 'CENTRO', 898, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `forma_entrega`
--

CREATE TABLE `forma_entrega` (
  `id_entrega` int(11) NOT NULL,
  `forma_entrega` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor_entrega` decimal(62,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `forma_entrega`
--

INSERT INTO `forma_entrega` (`id_entrega`, `forma_entrega`, `valor_entrega`) VALUES
(1, 'RETIRAR NO LOCAL', '0.00'),
(4, 'RECEBER EM CASA', '5.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens_pedido`
--

CREATE TABLE `itens_pedido` (
  `id_itens` int(11) NOT NULL,
  `id_itens_pedido` int(11) NOT NULL,
  `id_itens_cliente` int(11) NOT NULL,
  `id_itens_produto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `itens_pedido`
--

INSERT INTO `itens_pedido` (`id_itens`, `id_itens_pedido`, `id_itens_cliente`, `id_itens_produto`) VALUES
(1, 1, 1, 43);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `id_pedido` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `valor_total` decimal(62,2) NOT NULL,
  `forma_entrega` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=aberto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `id_cliente`, `valor_total`, `forma_entrega`, `status`) VALUES
(1, 1, '41.50', 4, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id_produto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `preco_normal` decimal(10,2) NOT NULL,
  `preco_promocional` decimal(10,2) DEFAULT NULL,
  `promocao` tinyint(1) DEFAULT NULL,
  `imagem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id_produto`, `id_categoria`, `nome`, `descricao`, `preco_normal`, `preco_promocional`, `promocao`, `imagem`) VALUES
(39, 29, 'Cerveja Sol Long Neck 330ml', 'Cerveja Sol Long Neck 330ml', '4.15', '3.50', 1, 'img/produtos/a34d76344f6f0f40f53e139eb8d6450e.jpg'),
(40, 29, 'Cerveja Budweiser Long Neck 330ml', 'Cerveja Budweiser Long Neck 330ml Desc', '4.00', '3.23', 1, 'img/produtos/985f01248b1c44209f7e99766581c270.jpg'),
(42, 33, 'Big Burguer + Fritas', '1un Hambúguer Big Bacon + 1 Porção de Batata Frita', '31.00', '27.50', 1, 'img/produtos/eaa664409e269ce999473707dcd62b49.jpg'),
(43, 33, 'Combo Buguer + Fritas  + Refrigerante', 'Combo Buguer + Fritas  + Refrigerante', '36.50', '31.00', 1, 'img/produtos/702e7e6de96c7b4fef50e75c9802e4c0.jpg'),
(59, 31, 'A La Minuta - 1 Porção', '1 bife (gado ou frango), Arroz, Feijão, Saladas, Bata Frita', '29.50', '25.00', 1, 'img/produtos/ee484e9b5ec17f8ab3e3f8eefe96292d.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `promocao`
--

CREATE TABLE `promocao` (
  `id_promocao` int(11) NOT NULL,
  `data_inicial` date NOT NULL,
  `data_final` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `promocao`
--

INSERT INTO `promocao` (`id_promocao`, `data_inicial`, `data_final`) VALUES
(3, '2019-10-01', '2019-10-05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indexes for table `forma_entrega`
--
ALTER TABLE `forma_entrega`
  ADD PRIMARY KEY (`id_entrega`);

--
-- Indexes for table `itens_pedido`
--
ALTER TABLE `itens_pedido`
  ADD PRIMARY KEY (`id_itens`);

--
-- Indexes for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id_produto`),
  ADD KEY `FK_Cat` (`id_categoria`);

--
-- Indexes for table `promocao`
--
ALTER TABLE `promocao`
  ADD PRIMARY KEY (`id_promocao`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `forma_entrega`
--
ALTER TABLE `forma_entrega`
  MODIFY `id_entrega` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `itens_pedido`
--
ALTER TABLE `itens_pedido`
  MODIFY `id_itens` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id_produto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `promocao`
--
ALTER TABLE `promocao`
  MODIFY `id_promocao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `FK_Cat` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
