<?php include_once 'cabecalho.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Validade Promocão</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Promoção</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <!-- ROW --> 
       <div class="row">
           <div class="col-md-6">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Cadastrar Data</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form id="form" enctype="multipart/form-data" role="form">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="form-row align-items-center">
                                <div class="col-sm-6">
                                <b>De:</b>
                                <input type="date" class="form-control mb-2" name="inicio" required>
                                </div>
                                <div class="col-sm-6">
                                <b>Até:</b>
                                <input type="date" class="form-control mb-2" name="fim" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-block bg-gradient-success">Salvar</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
           </div>
           <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Data Cadastrada</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>De:</th>
                            <th>Até:</th>
                            <th  style="width: 10px">AÇÕES</th>
                        </tr>
                    </thead>
                        <tbody id="tabela">
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                
           </div>
       </div>
       <!-- ROW --> 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  

  <script>
      //preenche as tabelas ao 
      $(document).ready(function(){
            popularTabela();
        });


        $('#form').submit(function(e){
            e.preventDefault();
            var form = $(this);
            salvarForm(form);
        })//fim submit

        //funcao chamada acima
        function salvarForm(dados){
            $.ajax({
            type:'POST',
            data:dados.serialize(),
            url:'pages/promocao/salvar_promocao.php'
            }).done(function(data){
            
            $sucesso = $.parseJSON(data)['sucesso'];
            $mensagem = $.parseJSON(data)['mensagem'];
            $erro = $.parseJSON(data)['mensagem'];
            console.log($erro);
            if($sucesso){
                alert($mensagem);
                location.reload();
            }else{
                alert($mensagem);
            } 

            }).fail(function(){
            alert('Erro interno. Contate o administrador');
            });
        }



        //função para popular a tabela 
        function popularTabela(){
            $('#tabela').empty(); //limpa a tabela
            $.ajax({
            type:'POST',
            dataType:'json',
            url:'pages/promocao/promocao_json.php'

            }).done(function(data){
            for(var i =0; data.length>i; i++){
            
                //preenche a tabela
                $('#tabela').append("<tr><td> "+data[i].data_inicial+" </td><td>  "+data[i].data_final+"</td><td> <a id='excluir' href='' value='"+data[i].id_promocao+"''><button class='btn'><i class='fa fa-trash fa-2x'></button></i></a> </td></tr>");
            }

            //evento de clique no botao de excluir
            $('#tabela tr td #excluir').click(function(e){
                e.preventDefault();
                var id = $(this).attr("value");
                var linha = $(this).parent().parent();
                exluirRegistro(id,linha);
            })  


            }).fail(function(){
            alert('erro interno. Contate o Administrador.')

            });
        }//fim função

        //FUNÇÃO DE EXCLUIR REGISTRO
        function exluirRegistro(cod, elemento){
            $.ajax({
            type:'POST',
            data:"id="+cod,
            url:'pages/promocao/excluir_promocao.php',
            asyn: true
            }).done(function(data){
            $sucesso = $.parseJSON(data)['sucesso'];
            $mensagem = $.parseJSON(data)['mensagem'];

            if($sucesso){
                elemento.fadeOut();

            }else{
                alert($mensagem);
            }

            }).fail(function(){
            alert('Erro interno. Contate o administrador');
            })
        }//fim função

  </script> 

<?php if(isset($_GET['edit']) && $_GET['edit'] == true){ ?>
  <script>alert('Alterado com sucesso!')</script>   
<?php }?>

<?php if(isset($_GET['save']) && $_GET['save'] == true){ ?>
  <script>alert('Cadastrado com sucesso!')</script>   
<?php }?>
  
<?php include_once 'rodape.php'; ?>

