<?php include_once 'cabecalho.php'; ?>




<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Categorias</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Categorias</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <!-- ROW --> 
       <div class="row">
           <div class="col-md-6">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Cadastrar Categoria</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form id="form" enctype="multipart/form-data" role="form">
                    <div class="card-body">
                    <div class="form-group">
                        <label ><b>Nome </b></label>
                        <input type="text" class="form-control" id="nome" name="nome"required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Imagem (600 x 600)</label>
                        <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="foto" name="foto" required>
                            <label class="custom-file-label" for="exampleInputFile">Selecionar arquivo</label>
                        </div>
                        
                        </div>
                    </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-block bg-gradient-success">Salvar</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
           </div>
           <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Categorias Cadastradas</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="width:30px">#</th>
                            <th>NOME</th>
                            <th  style="width: 10px">AÇÕES</th>
                        </tr>
                    </thead>
                        <tbody id="tabela">
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                
           </div>
       </div>
       <!-- ROW --> 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!--MODAL EDIÇÃO DE CATEGORIA-->
  <div class="modal fade" id="modalEditar">
    <div class="modal-dialog modal-lg">
      <div class="modal-content ">
        <div class="modal-header bg-primary">
          <h4 class="modal-title"><i class="fas fa-edit"></i> Editar Categoria</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <!-- general form elements disabled -->
          <div class="card card-warning">
            <div class="card-body">
              <form action="pages/categorias/salvar_edicao_categoria.php" method="POST" enctype="multipart/form-data" role="form">
                <div class="row">
                  <div class="col-sm-7">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Nome</label>
                      <input type="text" class="form-control" id="idModal" name="idModal" hidden>
                      <input type="text" class="form-control" id="nomeModal" name="nomeModal" >
                    </div>
                      <div class="col-sm-7">
                        <!-- textarea -->
                        <div class="form-group">
                            <div class="form-group">
                                <label for="exampleInputFile">Imagem (600 x 600)</label>
                                  <div class="input-group">
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="fotoModal" name="fotoModal">
                                      <label class="custom-file-label" for="exampleInputFile">Selecionar arquivo</label>
                                  </div>
                                  
                                  </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="ml-5 col-sm-5">
                        <img id="imagemModal"  width="200px">
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
          <!-- /.card -->
          <!-- general form elements disabled -->
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
      </form>
      </div></div></div>
    </div>
  </div>  
  <!-- END MODAL EDIÇÃO DE PRODUTOS-->

  <script>
      //preenche as tabelas ao 
      $(document).ready(function(){
           popularTabela();
        });


        $('#form').submit(function(e){
            e.preventDefault();
            var nome = $('#nome').val();
            var form = $('form')[0]; 
            var formData = new FormData(form);
            salvarForm(formData,nome);
        })//fim submit

        //funcao chamada acima
        function salvarForm(dados,nome){
            $.ajax({
                type:'POST',
                data:dados,
                url:'pages/categorias/salvar_categoria.php?nome='+nome,
                processData: false,  
                contentType: false
            }).done(function(data){
                $sucesso = $.parseJSON(data)['sucesso'];
                $mensagem = $.parseJSON(data)['mensagem'];
                $erro = $.parseJSON(data)['erro'];
                console.log($erro);
                if($sucesso){
                    alert('Cadastrado com sucesso!');
                    location.reload();
                }else{
                    alert('Falha ao efetuar cadastro!');
                } 

            }).fail(function(){
            alert('Erro interno. Contate o administrador');
            });
        }



        //função para popular a tabela 
        function popularTabela(){
            $('#tabela').empty(); //limpa a tabela
            $.ajax({
            type:'GET',
            dataType:'json',
            url:'pages/categorias/categorias_json.php'

            }).done(function(data){
            for(var i =0; data.length>i; i++){
            
            //preenche a tabela
            $('#tabela').append('<tr><td><img height="50" width="50"  src=../'+data[i].imagem_categoria+'></td><td>' +data[i].nome_categoria + '</td><td><a id="editar" alt="'+data[i].id_categoria+'"><button class="btn btn-sm btn-block bg-gradient-primary">Editar</button></a>  <a id="excluir" href="" alt="'+data[i].id_categoria+'"><button class="btn btn-block btn-sm  bg-gradient-danger">Excluir</button></a></td></tr>');
            }

            //evento de clique no botao de excluir
            $('#tabela tr td #excluir').click(function(e){
                e.preventDefault();
                var id = $(this).attr("alt");
                var linha = $(this).parent().parent();
                exluirRegistro(id,linha);
            })  

            //evento do botão editar 
            $('#tabela tr td #editar').click(function(e){
                e.preventDefault();
                var id = $(this).attr("alt");
                editarProduto(id);
            })

            }).fail(function(){
            alert('erro interno. Contate o Administrador.')

            });
        }//fim função

        //FUNÇÃO DE EXCLUIR REGISTRO
        function exluirRegistro(cod, elemento){
            $.ajax({
            type:'POST',
            data:"id="+cod,
            url:'pages/categorias/excluir_categoria.php',
            asyn: true
            }).done(function(data){
            $sucesso = $.parseJSON(data)['sucesso'];
            $mensagem = $.parseJSON(data)['mensagem'];

            if($sucesso){
                elemento.fadeOut();

            }else{
                alert($mensagem);
            }

            }).fail(function(){
            alert('Erro interno. Contate o administrador');
            })
        }//fim função


        //função popula modal editar produto
        function editarProduto(id){
            $.ajax({
            type:'POST',
            data:"id="+id,
            url:'pages/categorias/json_modal.php'
            }).done(function(data){
            var dados = $.parseJSON(data);
            $('#idModal').val(dados[0].id_categoria);
            $('#nomeModal').val(dados[0].nome_categoria);
            $('#imagemModal').attr('src','../'+dados[0].imagem_categoria);

            $('#modalEditar').modal();
            
            }).fail(function(){
            alert('erro interno. Contate o Administrador. 2°json')

            });
        }

  </script> 

<?php if(isset($_GET['edit']) && $_GET['edit'] == true){ ?>
  <script>alert('Alterado com sucesso!')</script>   
<?php }?>

<?php if(isset($_GET['save']) && $_GET['save'] == true){ ?>
  <script>alert('Cadastrado com sucesso!')</script>   
<?php }?>
  
<?php include_once 'rodape.php'; ?>

