<?php include_once 'cabecalho.php'; ?>
<?php 
    include_once('../database/conexao.php');
    $conn = getConncection();
    $stm = $conn->prepare("SELECT * FROM pedidos");
    $stm->execute();
    $count = $stm->rowCount();
?>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pedidos Recebidos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Pedidos Recebidos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="container">
      <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
          <!-- small card -->
          <div class="small-box bg-olive">
              <div class="inner">
                <h3><?php echo $count; ?></h3>

                <p>Pedidos Pendentes</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <a href="#" class="small-box-footer">
                 <br>
              </a>
            </div>
        </div>
        <div class="col-sm"></div>   
        <audio id="audio" autoplay>
            <source src="dist/audio/som1.mp3" type="audio/mp3" /> 
        </audio>
        <button id="botao" class="btn btn-success">Play</button>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <!-- ROW --> 
       <div class="row">
        <div class="col-12">
              <div class="card-orange">
                <div class="card-header">
                  <h3 class="card-title text-light">
                  <i class="mr-2 fas fa-list"></i>
                    Pedidos
                  </h3>
                  
                  <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <p class="card-title">Atualizando...</p>
                      <div class="spinner-border text-ligth ml-auto" role="status"></div>
                    </div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Pedido</th>
                        <th>Nome</th>
                        <th>Forma Pgto</th>
                        <th>Forma Envio</th>
                        <th>Data Pedido</th>
                        <th>Endereço</th>
                        <th>Status</th>
                        <th>Espera</th>
                      </tr>
                    </thead>
                    <tbody id="tabela">
                      
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
        </div>
        <!-- ROW --> 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  

  <script>
    $('#botao').on('click',function(){
      playsom();
    })

    $(document).ready(function(){
           popularTabela();
      });
    //função para popular a tabela 
    function popularTabela(){
        $('#tabela').empty(); //limpa a tabela
        $.ajax({
        type:'GET',
        dataType:'json',
        url:'pages/pedidos/pedidos_json.php'
        }).done(function(data){
          
            playsom();
          
        for(var i =0; data.length>i; i++){
          /*
                STATUS
                1=EM ABERTO,
                2=EM PREPARAÇÃO
                3=AGUARDANDO ENVIO
                4=ENTREGUE
          */
          var status = data[i].STATUSPEDIDO
          if(status == 1){status = '<td class="badge badge-secondary">Em aberto'}
          else if(status == 2){status = '<td class="badge badge-primary">Em preparação'}
          else if(status == 3){status = '<td class="badge badge-warning">Aguardando envio'}
          else if(status == 4){status = '<td class="badge badge-success">Concluído'}

          //    ESPERA
          var espera = data[i].ESPERA;
          var hora = espera.substr(1,2);
          var min = espera.substr(4,2);
          //preenche a tabela
          $('#tabela').append(
            '<tr><td class="text-success font-weight-bold">'+data[i].PEDIDO +
            '</td><td>'+data[i].CLIENTE +
            '</td><td>'+data[i].PAGAMENTO +
            '</td><td>'+data[i].ENTREGA +
            '</td><td>'+data[i].DATAPEDIDO +
            '</td><td>'+data[i].ENDERECO +
            '</td>'+status +
            '</td><td class="text-danger font-weight-bold"><i class="far fa-clock"></i>'+hora+'h'+ min+'m'+
            '</td></tr>');
        }

        }).fail(function(){
        alert('erro interno. Contate o Administrador.')

        });
      }//fim função

      //FUNÇÃO TOCAR SOM E ALERTA
      function playsom(){
        audio = new Audio("dist/audio/som1.mp3");
        audio.play();
    }


  </script> 

  
<?php include_once 'rodape.php'; ?>

