<?php include_once 'cabecalho.php'; ?>
<?php 
    include_once('../database/conexao.php');
    $conn = getConncection();
    $stm = $conn->prepare("SELECT * FROM categorias ORDER BY nome_categoria ASC");
    $stm->execute();

    //select categorias modal
    $stm2 = $conn->prepare("SELECT * FROM categorias ORDER BY nome_categoria ASC");
    $stm2->execute();
?>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Produtos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Produtos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <!-- ROW --> 
       <div class="row">
           <div class="col-md-6">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Cadastrar Produto</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="pages/produtos/salvar_produto.php" method="POST" enctype="multipart/form-data" role="form">
                    <div class="card-body">
                    <div class="form-group">
                        <label ><b>Nome </b></label>
                        <input type="text" class="form-control" id="preco_normal" name="nome"required >
                    </div>
                    <div class="form-group">
                        <label ><b>Descrição (opcional)</b></label>
                        <textarea class="form-control" name="desc" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputState"><b>Categoria</b></label>
                        <select id="inputState" class="form-control" name="categoria" id="categoria" required>
                            <?php while($linha = $stm->fetch(PDO::FETCH_ASSOC)){ ?>
                            <option value="<?php echo $linha['id_categoria'] ?>" ><?php echo $linha['nome_categoria'] ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputState"><b>Promoção</b></label>
                        <select id="inputState" class="form-control" name="promocao" id="promocao" required>
                            <option selected value="1">Sim</option>
                            <option value="0">Não</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputState"><b>Preço Normal</b></label>
                        <input type="number" class="form-control" id="preco_normal" name="preco_normal" step=".01" placeholder="R$" required>
                    </div>
                    <div class="form-group">
                        <label for="inputState"><b>Preço Promocional</b></label>
                        <input type="number" class="form-control" id="preco_promo" name="preco_promo" step=".01"  placeholder="R$">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Imagem (600 x 600)</label>
                        <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="foto" name="foto" required>
                            <label class="custom-file-label" for="exampleInputFile">Selecionar arquivo</label>
                        </div>
                        
                        </div>
                    </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-block bg-gradient-success">Salvar</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
           </div>
           <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Produtos Cadastrados</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>NOME</th>
                            <th>CATEGORIA</th>
                            <th>PREÇO NORMAL</th>
                            <th>PREÇO PROMO</th>
                            <th>AÇÕES</th>
                        </tr>
                    </thead>
                        <tbody id="tabelaPromo">
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                
           </div>
       </div>
       <!-- ROW --> 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!--MODAL EDIÇÃO DE PRODUTOS-->
  <div class="modal fade" id="modalEditar">
    <div class="modal-dialog modal-lg">
      <div class="modal-content ">
        <div class="modal-header bg-primary">
          <h4 class="modal-title"><i class="fas fa-edit"></i> Editar Produto</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <!-- general form elements disabled -->
          <div class="card card-warning">
            <div class="card-body">
              <form action="pages/produtos/salvar_edicao_produto.php" method="POST" enctype="multipart/form-data" role="form">
                <div class="row">
                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Nome</label>
                      <input type="text" class="form-control" id="idModal" name="idModal" hidden>
                      <input type="text" class="form-control" id="nomeModal" name="nomeModal" >
                    </div>
                  </div>
                  <div class="col-sm-6">
                      <label for="categoriaModal"><b>Categoria</b></label>
                      <select id="categoriaModal" class="form-control" name="categoriaModal" id="categoriaModal" >
                          <?php while($linha = $stm2->fetch(PDO::FETCH_ASSOC)){ ?>
                          <option value="<?php echo $linha['id_categoria'] ?>" ><?php echo $linha['nome_categoria'] ?></option>
                          <?php }?>
                      </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <!-- textarea -->
                    <div class="form-group">
                        <div class="form-group">
                            <label ><b>Descrição (opcional)</b></label>
                            <textarea class="form-control" name="descModal" id="descModal" rows="3"></textarea>
                          </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <label for="inputState"><b>Promoção</b></label>
                    <select id="inputState" class="form-control" name="promocaoModal" id="promocaoModal" >
                        <option selected value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                          <div class="form-group">
                              <label for="inputState"><b>Preço Normal</b></label>
                              <input type="number" class="form-control" id="preco_normal_modal" name="preco_normal_modal" step=".01" placeholder="R$" > 
                          </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                        <label for="inputState"><b>Preço Promocional</b></label>
                        <input type="number" class="form-control" id="preco_promo_modal" name="preco_promo_modal" step=".01"  placeholder="R$">
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                            <div class="form-group">
                                <label for="exampleInputFile">Imagem (600 x 600)</label>
                                  <div class="input-group">
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="fotoModal" name="fotoModal">
                                      <label class="custom-file-label" for="exampleInputFile">Selecionar arquivo</label>
                                  </div>
                                  
                                  </div> 
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                          <img id="imagemModal"  width="200px">
                      </div>
                    </div>
              
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <!-- general form elements disabled -->
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
      </form>
      </div>
    </div>
  </div>  
  <!-- END MODAL EDIÇÃO DE PRODUTOS-->

  <script>
      //preenche as tabelas ao 
      $(document).ready(function(){
        popularTabelaPromocao(1);
        popularTabelaGeral(0);
      });

    

      //função para popular a tabela de produtos em PROMOÇÃO
      function popularTabelaPromocao(promo){
        $('#tabelaPromo').empty(); //limpa a tabela
        $.ajax({
          type:'GET',
          dataType:'json',
          url:'pages/produtos/produtos_json.php?promo=' + promo

        }).done(function(data){
          for(var i =0; data.length>i; i++){
            var v = data[i].preco_normal;
            var v2 = data[i].preco_promocional;
            var vNormal = v.replace('.',',');
            var vPromo = v2.replace('.',',');

            var idCripto = "9651235485"+data[i].id_produto+"0654794713";
            //preenche a tabela
            $('#tabelaPromo').append('<tr><td><img height="42" width="42" src=../'+data[i].imagem +'>' +'</td><td>' +data[i].nome + '</td><td>' +data[i].nome_categoria + '</td><td>R$' +vNormal + '</td><td>R$'+ vPromo +'</td><td><a id="editarPromo" alt="'+data[i].id_produto+'"><button class="btn btn-sm btn-block bg-gradient-primary">Editar</button></a>  <a id="excluirPromo" href="" alt="'+data[i].id_produto+'"><button class="btn btn-block btn-sm  bg-gradient-danger">Excluir</button></a></td></tr>');
          }

           //evento de clique no botao de excluir
          $('#tabelaPromo tr td #excluirPromo').click(function(e){
            e.preventDefault();
            var id = $(this).attr("alt");
            var linha = $(this).parent().parent();
            exluirRegistro(id,linha);
          })

          //evento do botão editar 
          $('#tabelaPromo tr td #editarPromo').click(function(e){
            e.preventDefault();
            var id = $(this).attr("alt");
            editarProduto(id);
          })


        }).fail(function(){
          alert('erro interno. Contate o Administrador. 1° json')

        });
      }//fim função



      //função para popular a tabela de produtos em geral
      function popularTabelaGeral(promo){
        $('#tabelaGeral').empty(); //limpa a tabela
        $.ajax({
          type:'GET',
          dataType:'json',
          url:'pages/produtos/produtos_json.php?promo=' + promo

        }).done(function(data){
          for(var i =0; data.length>i; i++){
            var v = data[i].preco_normal;
            var vNormal = v.replace('.',',');

            var idCripto = "9651235485"+data[i].id_produto+"0654794713";
   
            //preenche a tabela
            $('#tabelaGeral').append('<tr><td><img height="42" width="42" src='+data[i].imagem +'>' +'</td><td>' +data[i].nome + '</td><td>' +data[i].nome_categoria + '</td><td>R$' +vNormal +'</td><td> <a  href="cpanel_editar_produto.php?id='+idCripto+'"><button class="btn"><i class="fa fa-pencil" aria-hidden="true"></i></button></a> <a id="excluirProd" href="" alt="'+data[i].id_produto+'"><button class="btn"><i class="fa fa-trash" aria-hidden="true"></button></a></td></tr>');
          }

          //evento de clique no botao de excluir
          $('#tabelaGeral tr td #excluirProd').click(function(e){
            e.preventDefault();
            var id = $(this).attr("alt");
            var linha = $(this).parent().parent();
            exluirRegistro(id,linha);
          })


        }).fail(function(){
          alert('erro interno. Contate o Administrador. 2°json')

        });
      }//fim função
      

      //FUNÇÃO DE EXCLUIR REGISTRO
      function exluirRegistro(cod, elemento){
        $.ajax({
          type:'POST',
          data:"id="+cod,
          url:'pages/produtos/excluir_produto.php',
          asyn: true
        }).done(function(data){
          $sucesso = $.parseJSON(data)['sucesso'];
          $mensagem = $.parseJSON(data)['mensagem'];

          if($sucesso){
            elemento.fadeOut();

          }else{
            alert($mensagem);
          }

        }).fail(function(){
          alert('Erro interno. Contate o administrador.Excluir');
        })
      }//fim função

      //função popula modal editar produto
      function editarProduto(id){
        $.ajax({
          type:'POST',
          data:"id="+id,
          url:'pages/produtos/json_modal.php'
        }).done(function(data){
          var dados = $.parseJSON(data);
          $('#idModal').val(dados[0].id_produto);
          $('#nomeModal').val(dados[0].nome);
          $('#categoriaModal').val(dados[0].id_categoria);
          $('#descModal').val(dados[0].descricao);
          $('#promocaoModal').val(dados[0].promocao);
          $('#preco_normal_modal').val(dados[0].preco_normal);
          $('#preco_promo_modal').val(dados[0].preco_promocional);
          $('#imagemModal').attr('src','../'+dados[0].imagem);

          $('#modalEditar').modal();
          
        }).fail(function(){
          alert('erro interno. Contate o Administrador. 2°json')

        });
      }

      

  </script> 

<?php if(isset($_GET['edit']) && $_GET['edit'] == true){ ?>
  <script>alert('Alterado com sucesso!')</script>   
<?php }?>

<?php if(isset($_GET['save']) && $_GET['save'] == true){ ?>
  <script>alert('Cadastrado com sucesso!')</script>   
<?php }?>
  
<?php include_once 'rodape.php'; ?>

