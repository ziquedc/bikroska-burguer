<?php
include_once('../../../database/conexao.php');

$id = $_POST['idModal'];
$name = $_POST['nomeModal'];
$desc = $_POST['descModal'];
$categoria = (int) $_POST['categoriaModal'];
$preco_normal = (float) $_POST['preco_normal_modal'];
$preco_promo = (float) $_POST['preco_promo_modal'];
$promocao = $_POST['promocaoModal'];

//SELECT PARA PEGAR O CAMINHO DA IMAGEM PARA EXCLUI A ATUAL E DEPOIS SALVAR A NOVA CADASTRADA
$conn = getConncection();
$stm2 = $conn->prepare(
    "SELECT * 
    FROM  produtos
     WHERE  id_produto = ?");
$stm2->bindParam(1,$id);
$stm2->execute();
$dados = $stm2->fetch(PDO::FETCH_ASSOC);
$imagemAtual = $dados['imagem'];
//array de retorno para o ajax
$retorno = array();

//com imagem
if($_FILES['fotoModal']['size'] > 0){ //se o tamanho do arquivo for maior que zero, significa que foi inserida uma imagem
    $imagem = $_FILES['fotoModal'];
    //PROCESSAR IMAGEM
    $largura = 5000;
    $altura = 5000;
    $tamanho = 10485760;

    $error = array();
    // Verifica se o arquivo é uma imagem
    if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $imagem["type"])){
        $error[1] = "Isso não é uma imagem.";
        } 
    // Pega as dimensões da imagem
    $dimensoes = getimagesize($imagem["tmp_name"]);

    // Verifica se a largura da imagem é maior que a largura permitida
    if($dimensoes[0] > $largura) {
        $error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
    }

    // Verifica se a altura da imagem é maior que a altura permitida
    if($dimensoes[1] > $altura) {
        $error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
    }

    // Verifica se o tamanho da imagem é maior que o tamanho permitido
    if($imagem["size"] > $tamanho) {
    $error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
    }  

    // Se não houver nenhum erro
    if (count($error) == 0) {
            
        // Pega extensão da imagem
        preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $imagem["name"], $ext);

        // Gera um nome único para a imagem
        $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

        // Caminho de onde ficará a imagem
        $caminho_imagem = "../../../img/produtos/" . $nome_imagem;
        $caminho_imagem_banco = "img/produtos/" . $nome_imagem;

        //exclui a imagem atual do diretorio
        unlink('../../../'.$imagemAtual);

        // Faz o upload da imagem para seu respectivo caminho
        move_uploaded_file($imagem["tmp_name"], $caminho_imagem);
    
        //SALVAR NO BANCO
        $conn = getConncection();
        $stm = $conn->prepare(
            "UPDATE produtos 
            SET id_categoria = ?, nome = ?, descricao = ? ,preco_normal = ?, preco_promocional = ?, promocao = ?,imagem = ? 
            WHERE  id_produto = ?");

        $stm->bindParam(1,$categoria);
        $stm->bindParam(2,$name);
        $stm->bindParam(3,$desc);
        $stm->bindParam(4,$preco_normal);
        $stm->bindParam(5,$preco_promo);
        $stm->bindParam(6,$promocao);
        $stm->bindParam(7,$caminho_imagem_banco);
        $stm->bindParam(8,$id);

        

        if($stm->execute()){
            //echo "<script>alert('Alterado com sucesso');</script>"; 
            echo "<script>window.location = '../../produtos.php?edit=true';</script>";

        }else{
            echo "<script>alert('Falha ao alterar');</script>"; 
            echo "<script>window.location = '../../produtos.php';</script>";
        }
    }

    // Se houver mensagens de erro, exibe-as
    if (count($error) != 0) {
        foreach ($error as $erro) {
            echo $erro . "<br />";
        }
    }

}else{//sem imagem
    //SALVAR NO BANCO
    $conn = getConncection();
    $stm = $conn->prepare(
        "UPDATE produtos 
        SET id_categoria = ?, nome = ?, descricao = ? ,preco_normal = ?, preco_promocional = ?, promocao = ?
        WHERE  id_produto = ?");

        $stm->bindParam(1,$categoria);
        $stm->bindParam(2,$name);
        $stm->bindParam(3,$desc);
        $stm->bindParam(4,$preco_normal);
        $stm->bindParam(5,$preco_promo);
        $stm->bindParam(6,$promocao);
        $stm->bindParam(7,$id);

        

        if($stm->execute()){
            //echo "<script>alert('Alterado com sucesso');</script>"; 
            echo "<script>window.location = '../../produtos.php?edit=true';</script>";

        }else{
            echo "<script>alert('Falha ao alterar');</script>"; 
            echo "<script>window.location = '../../produtos.php';</script>";
        }
    }

   















?>