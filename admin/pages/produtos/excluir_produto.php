<?php
include_once('../../../database/conexao.php');

$id = $_POST['id'];

$conn = getConncection();
$stm = $conn->prepare('SELECT * FROM produtos WHERE id_produto = ?');
$stm->bindParam(1, $id);
$stm->execute();
$dados = $stm->fetch(PDO::FETCH_ASSOC);
$imagem = $dados['imagem'];

//faz o delete
$stm2 = $conn->prepare('DELETE FROM produtos WHERE id_produto = ?');
$stm2->bindParam(1,$id);
unlink('../../../'.$imagem);

//array de retorno
$retorno = array();

if($stm2->execute()){
	$retorno['sucesso'] = true;
	$retorno['mensagem'] = "Produto Excluído com sucesso";
}else{
	$retorno['sucesso'] = false;
	$retorno['mensagem'] = "Falha ao excluir";
}

echo json_encode($retorno);

?>