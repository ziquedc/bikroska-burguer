<?php 
//qualquer domino possa acessar o arquivo. Uma api RESt JSON por exemplo
header('Access-Control-Allow-Origin:*');
include_once('../../../database/conexao.php');
$conn = getConncection();
$stm = $conn->prepare(
    'SELECT 
    id_pedido PEDIDO,
    nome_cliente CLIENTE,
    forma_pagamento PAGAMENTO,
    forma_entrega.forma_entrega ENTREGA,
    DATE_FORMAT(data_pedido,"%d/%m/%Y  %H:%i:%s") DATAPEDIDO,
    CONCAT(rua_cliente,", " ,numero_cliente,"- " ,bairro_cliente) ENDERECO,
    pedidos.status STATUSPEDIDO,
    TIMEDIFF(data_pedido,NOW()) ESPERA
    FROM pedidos 
    INNER JOIN itens_pedido 
    ON pedidos.id_pedido = itens_pedido.id_itens_pedido
    INNER JOIN produtos 
    ON itens_pedido.id_itens_produto = produtos.id_produto
    INNER JOIN forma_pagamento 
    ON pedidos.id_forma_pagamento = forma_pagamento.id_pagamento
    INNER JOIN forma_entrega
    ON pedidos.forma_entrega = forma_entrega.id_entrega
    INNER JOIN cliente
    ON pedidos.id_cliente = cliente.id_cliente
    ORDER BY PEDIDO DESC');
$stm->execute();
$count = $stm->rowCount();
//array de retorno do json
$retorno = array();

while($linha = $stm->fetch(PDO::FETCH_ASSOC)){
	$retorno[] =$linha;
}
echo json_encode($retorno);

?>