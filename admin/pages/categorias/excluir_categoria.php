<?php
include_once('../../../database/conexao.php');

$id = $_POST['id'];

//select para pegar o caminho da imagem atual e excluir
$conn = getConncection();
$stm = $conn->prepare('SELECT * FROM categorias WHERE id_categoria = ?');
$stm->bindParam(1, $id);
$stm->execute();
$dados = $stm->fetch(PDO::FETCH_ASSOC);
$imagem = $dados['imagem_categoria'];

//faz o delete
$stm2 = $conn->prepare('DELETE FROM categorias WHERE id_categoria = ?');
$stm2->bindParam(1,$id);
unlink('../../../'.$imagem);

//array de retorno
$retorno = array();

if($stm2->execute()){
	$retorno['sucesso'] = true;
	$retorno['mensagem'] = "categoria Excluída com sucesso";
}else{
	$retorno['sucesso'] = false;
	$retorno['mensagem'] = "Falha ao excluir";
}

echo json_encode($retorno);

?>