<?php
include_once('../../../database/conexao.php');

$id = $_POST['idModal'];
$name = $_POST['nomeModal'];

//SELECT PARA PEGAR O CAMINHO DA IMAGEM PARA EXCLUI A ATUAL E DEPOIS SALVAR A NOVA CADASTRADA
$conn = getConncection();
$stm2 = $conn->prepare(
    "SELECT * 
    FROM  categorias
     WHERE  id_categoria = ?");
$stm2->bindParam(1,$id);
$stm2->execute();
$dados = $stm2->fetch(PDO::FETCH_ASSOC);
$imagemAtual = $dados['imagem_categoria'];
//array de retorno para o ajax
$retorno = array();

//com imagem
if($_FILES['fotoModal']['size'] > 0){ //se o tamanho do arquivo for maior que zero, significa que foi inserida uma imagem
    $imagem = $_FILES['fotoModal'];
    //PROCESSAR IMAGEM
    $largura = 5000;
    $altura = 5000;
    $tamanho = 10485760;

    $error = array();
    // Verifica se o arquivo é uma imagem
    if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $imagem["type"])){
        $error[1] = "Isso não é uma imagem.";
        } 
    // Pega as dimensões da imagem
    $dimensoes = getimagesize($imagem["tmp_name"]);

    // Verifica se a largura da imagem é maior que a largura permitida
    if($dimensoes[0] > $largura) {
        $error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
    }

    // Verifica se a altura da imagem é maior que a altura permitida
    if($dimensoes[1] > $altura) {
        $error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
    }

    // Verifica se o tamanho da imagem é maior que o tamanho permitido
    if($imagem["size"] > $tamanho) {
    $error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
    }  

    // Se não houver nenhum erro
    if (count($error) == 0) {
            
        // Pega extensão da imagem
        preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $imagem["name"], $ext);

        // Gera um nome único para a imagem
        $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

        // Caminho de onde ficará a imagem
        $caminho_imagem = "../../../img/categorias/" . $nome_imagem;
        $caminho_imagem_banco = "img/categorias/" . $nome_imagem;

        //exclui a imagem atual do diretorio
        unlink('../../../'.$imagemAtual);

        // Faz o upload da imagem para seu respectivo caminho
        move_uploaded_file($imagem["tmp_name"], $caminho_imagem);
    
        //SALVAR NO BANCO
        $conn = getConncection();
        $stm = $conn->prepare(
            "UPDATE categorias 
            SET nome_categoria = ? ,imagem_categoria = ? 
            WHERE  id_categoria = ?");

        $stm->bindParam(1,$name);
        $stm->bindParam(2,$caminho_imagem_banco);
        $stm->bindParam(3,$id);

        

        if($stm->execute()){
            //echo "<script>alert('Alterado com sucesso');</script>"; 
            echo "<script>window.location = '../../categorias.php?edit=true';</script>";

        }else{
            echo "<script>alert('Falha ao alterar');</script>"; 
            echo "<script>window.location = '../../categorias.php';</script>";
        }
    }

    // Se houver mensagens de erro, exibe-as
    if (count($error) != 0) {
        foreach ($error as $erro) {
            echo $erro . "<br />";
        }
    }

}else{//sem imagem
    //SALVAR NO BANCO
    $conn = getConncection();
    $stm = $conn->prepare(
        "UPDATE categorias 
        SET nome_categoria = ?
        WHERE  id_categoria = ?");

        $stm->bindParam(1,$name);
        $stm->bindParam(2,$id);

        

        if($stm->execute()){
            //echo "<script>alert('Alterado com sucesso');</script>"; 
            echo "<script>window.location = '../../categorias.php?edit=true';</script>";

        }else{
            echo "<script>alert('Falha ao alterar');</script>"; 
            echo "<script>window.location = '../../categorias.php';</script>";
        }
    }

   















?>