<?php
include_once('../../../database/conexao.php');


$imagem = $_FILES['foto'];


//array de retorno para o ajax
$retorno = array();


//PROCESSAR IMAGEM
$largura = 2000;
$altura = 800;
$tamanho = 10485760;

$error = array();
// Verifica se o arquivo é uma imagem
if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $imagem["type"])){
    $error[1] = "Isso não é uma imagem.";
    } 
// Pega as dimensões da imagem
$dimensoes = getimagesize($imagem["tmp_name"]);

// Verifica se a largura da imagem é maior que a largura permitida
if($dimensoes[0] > $largura) {
    $error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
}

// Verifica se a altura da imagem é maior que a altura permitida
if($dimensoes[1] > $altura) {
    $error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
}

// Verifica se o tamanho da imagem é maior que o tamanho permitido
if($imagem["size"] > $tamanho) {
   $error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
}  

// Se não houver nenhum erro
if (count($error) == 0) {
		
    // Pega extensão da imagem
    preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $imagem["name"], $ext);

    // Gera um nome único para a imagem
    $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

    // Caminho de onde ficará a imagem
    $caminho_imagem = "../../../img/banner/" . $nome_imagem;
    $caminho_imagem_banco = "img/banner/" . $nome_imagem;
    

    // Faz o upload da imagem para seu respectivo caminho
    move_uploaded_file($imagem["tmp_name"], $caminho_imagem);

    
   
    
    //SALVAR NO BANCO
	$conn = getConncection();
	$stm = $conn->prepare("INSERT INTO banner (caminho) VALUES(?)");

	$stm->bindParam(1,$caminho_imagem_banco);
	

	$retorno = array();

    if($stm->execute()){
        //echo "<script>alert('Cadastrado com sucesso');</script>"; 
        //echo "<script>window.location = 'cpanel_banner.php';</script>";
        $retorno['sucesso'] = true;
        $retorno['mensagem'] = "Banner salvo com sucesso";
    }else{
        $retorno['sucesso'] = false;
        $retorno['mensagem'] = "Erro ao salvar banner";
    }
}

// Se houver mensagens de erro, exibe-as
if (count($error) != 0) {
    foreach ($error as $erro) {
        $retorno['erro'] = true;
        $retorno['mensagem'] = $erro;
        
    }
}
echo json_encode($retorno);
?>