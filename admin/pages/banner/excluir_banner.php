<?php
include_once('../../../database/conexao.php');

$id = $_POST['id'];

$conn = getConncection();
$stm = $conn->prepare('SELECT * FROM banner WHERE id_banner = ?');
$stm->bindParam(1, $id);
$stm->execute();
$dados = $stm->fetch(PDO::FETCH_ASSOC);
$imagem = $dados['caminho'];

//faz o delete
$conn = getConncection();
$stm2 = $conn->prepare('DELETE FROM banner WHERE id_banner = ?');
$stm2->bindParam(1,$id);
unlink('../../../'.$imagem);
//array de retorno
$retorno = array();

if($stm2->execute()){
	$retorno['sucesso'] = true;
	$retorno['mensagem'] = "Banner excluído com sucesso";
}else{
	$retorno['sucesso'] = false;
	$retorno['mensagem'] = "Falha ao excluir";
}

echo json_encode($retorno);

?>