<?php
include_once('../../../database/conexao.php');

$id = $_POST['id'];


//faz o delete
$conn = getConncection();
$stm2 = $conn->prepare('DELETE FROM promocao WHERE id_promocao = ?');
$stm2->bindParam(1,$id);

//array de retorno
$retorno = array();

if($stm2->execute()){
	$retorno['sucesso'] = true;
	$retorno['mensagem'] = " Excluído com sucesso";
}else{
	$retorno['sucesso'] = false;
	$retorno['mensagem'] = "Falha ao excluir";
}

echo json_encode($retorno);

?>