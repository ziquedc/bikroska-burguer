<?php 
//qualquer domino possa acessar o arquivo. Uma api RESt JSON por exemplo
header('Access-Control-Allow-Origin:*');

include_once('../../../database/conexao.php');



$conn = getConncection();
$stm = $conn->prepare('SELECT id_promocao, DATE_FORMAT(data_inicial, "%d/%m/%Y") as data_inicial, DATE_FORMAT(data_final, "%d/%m/%Y") as data_final FROM promocao');
$stm->execute();

//array de retorno do json
$retorno = array();

while($linha = $stm->fetch(PDO::FETCH_ASSOC)){
	
	$retorno[] =$linha;
}

echo json_encode($retorno);


?>