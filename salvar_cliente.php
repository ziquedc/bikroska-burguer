<?php
include_once('conexao.php');

$nome = $_POST['nome'];
$cpf = $_POST['cpf'];
$nascimento = $_POST['nascimento'];
$telefone = $_POST['telefone'];
$email = $_POST['email'];
$senha = $_POST['senha'];
$cep = $_POST['cep'];
$cidade = $_POST['cidade'];
$uf = $_POST['uf'];
$rua = $_POST['rua'];
$bairro = $_POST['bairro'];
$numero = $_POST['numero'];
if(isset($_POST['complemento'])){
    $complemento = $_POST['complemento'];
}else{
    $complemento = "";
} 


 //SALVAR NO BANCO
$conn = getConncection();
$stm = $conn->prepare("INSERT cliente (nome_cliente, cpf_cliente, nascimento_cliente, telefone_cliente, email_cliente, senha_cliente, cep_cliente, cidade_cliente, uf_cliente, rua_cliente, bairro_cliente, numero_cliente, complemento_cliente) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
$stm->bindParam(1,$nome);
$stm->bindParam(2,$cpf);
$stm->bindParam(3,$nascimento);
$stm->bindParam(4,$telefone);
$stm->bindParam(5,$email);
$stm->bindParam(6,$senha);
$stm->bindParam(7,$cep);
$stm->bindParam(8,$cidade);
$stm->bindParam(9,$uf);
$stm->bindParam(10,$rua);
$stm->bindParam(11,$bairro);
$stm->bindParam(12,$numero);
$stm->bindParam(13,$complemento);

$retorno = array();

if($stm->execute()){
	$retorno['sucesso'] = true;
	$retorno['mensagem'] ="Cadastro realizado com sucesso.";

}
else{
	$retorno['sucesso'] = false;
	$retorno['mensagem'] ="Erro ao efetuar cadastro.";
}


echo json_encode($retorno);

?>