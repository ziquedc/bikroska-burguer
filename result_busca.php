<?php include('cabecalho.php');?>

<?php 
include_once('conexao.php');

//select nos produtos baseado no que for digitado na barra de pesquisa
$conn = getConncection();
$busca = (string) strtoupper($_POST['busca']);
$busca_ = '%'.$busca.'%';
$stm = $conn->prepare("SELECT * FROM produtos, categorias WHERE nome LIKE ? and produtos.id_categoria = categorias.id_categoria AND promocao = 1");
$stm->bindParam(1, $busca_);
$stm->execute();

$count = $stm->rowCount(); //numero de linhas retornas na consulta

//select nas categorias
$stm2 = $conn->prepare("SELECT * FROM categorias ORDER BY nome_categoria ASC");
$stm2->execute();


?>



<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- ASIDE -->
					<div id="aside" class="col-md-3">
						<!-- aside Widget -->
						<div class="aside">
							<h3 class="aside-title">Categorias</h3>
							<?php while($linha = $stm2->fetch(PDO::FETCH_ASSOC)){?>
							<div class="checkbox-filter">
								<div class="input-checkbox">
									<a href=""><label for="category-1">
										<span></span>
										<?php echo $linha['nome_categoria'] ?>
									</label></a>
								</div>
							</div>
							<?php }?>
						</div>
						<!-- /aside Widget -->


						

						
					</div>
					<!-- /ASIDE -->

					<!-- STORE -->
					<div id="store" class="col-md-9">
					<?php if($count > 0) {?>	
						<p class="h3"> <b>RESULTADO DE BUSCA PARA "<?php echo $busca; ?>"</b></p>
					<?php } ?>			
					<?php if($count == 0) {?>	
						<p class="h3"> <b>NENHUM RESULTADO ENCONTRADO PARA "<?php echo $busca; ?>"</b></p>
					<?php } ?>
						<!-- store products -->
						<div class="row">

							<?php while($linha = $stm->fetch(PDO::FETCH_ASSOC)){
								$preco_normal = str_replace('.',',', $linha['preco_normal']);
								$preco_promo = str_replace('.',',', $linha['preco_promocional']);
							?>
							<!-- product -->
							<div class="col-md-4 col-xs-6">
								<div class="product">
									<div class="product-img">
										<img src="<?php echo $linha['imagem'] ?>" alt="">
										<div class="product-label">
											
										</div>
									</div>
									<div class="product-body">
										<p class="product-category"><?php echo $linha['nome_categoria'] ?></p>
										<h3 class="product-name"><a href="#"><?php echo $linha['nome'] ?></a></h3>
										<h4 class="product-price">R$<?php echo $preco_promo; ?> <del class="product-old-price">R$<?php echo $preco_normal; ?></h4>
										
									</div>
									<div class="add-to-cart">
										<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Venha conferir</button>
									</div>
								</div>
							</div>
							<!-- /product -->
						<?php }?>
							

						
					</div>
					<!-- /STORE -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->







<?php include('rodape.php') ?>