<?php
include_once('conexao.php');
$conn = getConncection();
$stm = $conn->prepare("SELECT * FROM categorias");
$stm->execute();

//OBTEM OS DADOS DO PRODUTO ATRAVÉS DO ID RECEBIDO VIA GET
$id = $_GET['id'];
$id = str_replace("9651235485", "", $id);
$id = str_replace("0654794713", "", $id); 

$stm2 = $conn->prepare("SELECT * FROM produtos, categorias WHERE id_produto = ? AND categorias.id_categoria = produtos.id_categoria ");
$stm2->bindParam(1,$id);
$stm2->execute();
$dados = $stm2->fetch(PDO::FETCH_ASSOC);
?>
<?php include('cpanel_cabecalho.php') ?>
       <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
          
          <!--FORMULÁRIO DE EDIÇÃO DE PRODUTOS-->
          <div class="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col col-md-12" >
                <p class="h3 text-secondary" style="margin: 15px"> Editar Produto</p>
                <hr>
               <form action="salvar_edicao_produto.php" method="POST" enctype="multipart/form-data" style="margin-top: 30px">
                  
                  <input type="text" name="id" value="<?php echo $id ?>" hidden>
                  <div class="form-group">
                    <label ><b>Nome (descrição)</b></label>
                    <input type="text" class="form-control" id="preco_normal" name="nome"required value="<?php echo $dados['nome'] ?>">
                  </div>
                  
                  <div class="form-row">
                    <div class="form-group col-md-3">
                      <label for="inputState"><b>Categoria</b></label>
                      <select id="inputState" class="form-control" name="categoria" id="categoria" required>
                        <?php while($linha = $stm->fetch(PDO::FETCH_ASSOC)){ ?>
                          <option value="<?php echo $linha['id_categoria'] ?>" ><?php echo $linha['nome_categoria'] ?></option>
                        <?php }?>
                          <option value="<?php echo $dados['id_categoria'] ?>" selected> <?php echo $dados['nome_categoria'] ?> </option>
                      </select>
                    </div>
                    
                    <div class="form-group col-md-3">
                      <label for="inputState"><b>Promoção</b></label>
                      <select id="inputState" class="form-control" name="promocao" id="promocao" required="" >
                        <?php if($dados['promocao'] == 1) {?>
                          <option selected value="1">Sim</option>
                          <option value="0">Não</option>
                        <?php }else{ ?>
                          <option value="1">Sim</option>
                          <option selected value="0">Não</option>
                        <?php } ?>  

                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="inputState"><b>Preço Normal</b></label>
                      <input type="number" class="form-control" id="preco_normal" name="preco_normal" step=".01" placeholder="R$" value="<?php echo $dados['preco_normal'] ?>" required>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="inputState"><b>Preço Promocional</b></label>
                      <input type="number" class="form-control" id="preco_promo" name="preco_promo" step=".01"  placeholder="R$" value="<?php echo $dados['preco_promocional'] ?>">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-3">
                      <img id="foto" src="<?php echo $dados['imagem'] ?>" alt="Smiley face" height="150" width="150">
                    </div>
                    <div class="form-group col-md-5">
                      <div class="form-check">
                        <label for="exampleFormControlFile1"><b>Imagem (600 x 600)</b></label>
                        <input type="file" class="form-control-file" id="foto" name="foto" >
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-2">
                      <button type="submit" class="btn btn-primary">Salvar Edição</button>
                    </div>
                    <div class="form-group col-md-5">
                      <div id="msgRetorno" class="alert" role="alert"></div>
                    </div>
                  </div>
                </form>
          </div>


         
        </div>
        
      </main>
    </div>
    <script>
     


    </script>







    <!-- jQuery Plugins -->
    
    <script src="_js/bootstrap.min.js"></script>
    <script src="_js/slick.min.js"></script>
    <script src="_js/nouislider.min.js"></script>
    <script src="_js/jquery.zoom.min.js"></script>
    <script src="_js/main.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  </body>
  </html>
  