<?php
include_once('conexao.php');
$conn = getConncection();
$stm = $conn->prepare("SELECT * FROM categorias ORDER BY nome_categoria ASC");
$stm->execute();


?>
<?php include('cpanel_cabecalho.php') ?>


       <main class="mdl-layout__content mdl-color--grey-100">

        <div class="mdl-grid demo-content">
          
          <!--FORMULÁRIO DE CADASTRO DE PRODUTOS-->
          <div class="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col col-md-12" >
                <p class="h3 text-secondary" style="margin: 15px"> Cadastrar Produto</p>
                <hr>
               <form action="salvar_produto.php" method="POST" enctype="multipart/form-data" style="margin-top: 30px">
                  
                  <div class="form-group">
                    <label ><b>Nome (descrição)</b></label>
                    <input type="text" class="form-control" id="preco_normal" name="nome"required >
                  </div>
                  <div class="form-group">
                    <label ><b>Descrição (opcional)</b></label>
                    <textarea class="form-control" name="desc" rows="3"></textarea>
                  </div>
                  
                  <div class="form-row">
                    <div class="form-group col-md-3">
                      <label for="inputState"><b>Categoria</b></label>
                      <select id="inputState" class="form-control" name="categoria" id="categoria" required>
                        <?php while($linha = $stm->fetch(PDO::FETCH_ASSOC)){ ?>
                          <option value="<?php echo $linha['id_categoria'] ?>" ><?php echo $linha['nome_categoria'] ?></option>
                        <?php }?>
                      </select>
                    </div>
                    
                    <div class="form-group col-md-3">
                      <label for="inputState"><b>Promoção</b></label>
                      <select id="inputState" class="form-control" name="promocao" id="promocao" required>
                        <option selected value="1">Sim</option>
                        <option value="0">Não</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="inputState"><b>Preço Normal</b></label>
                      <input type="number" class="form-control" id="preco_normal" name="preco_normal" step=".01" placeholder="R$" required>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="inputState"><b>Preço Promocional</b></label>
                      <input type="number" class="form-control" id="preco_promo" name="preco_promo" step=".01"  placeholder="R$">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="form-check">
                      <label for="exampleFormControlFile1"><b>Imagem (600 x 600)</b></label>
                      <input type="file" class="form-control-file" id="foto" name="foto" required>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-2">
                      <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    <div class="form-group col-md-5">
                      <div id="msgRetorno" class="alert" role="alert"></div>
                    </div>
                  </div>
                </form>
          </div>

          
          <!--TABELA DE LISTAGEM DE PRODUTOS EM PROMOÇÃO-->
          <div class="demo-graphs mdl-shadow--2dp mdl-color--white mdl-cell mdl-cell--12-col" style="margin-top: 40px">
            
            <p class="h3 text-secondary" style="margin: 15px"> Produtos em Promoção Casdastrados </p>
            <hr>
            
            <table class="table table-striped">
              <thead class="">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col"><b>Nome</b></th>
                  <th scope="col"><b>Categoria</b></th>
                  <th scope="col"><b>Preço Normal</b></th>
                  <th scope="col"><b>Preço Promocional</b></th>
                  <th scope="col"><b>Ação</b></th>
                </tr>
              </thead>
              <tbody id="tabelaPromo">
             
                
              </tbody>
            </table>
          </div>

          <!--TABELA DE LISTAGEM DE PRODUTOS -->
          <div class="demo-graphs mdl-shadow--2dp mdl-color--white mdl-cell mdl-cell--12-col" style="margin-top: 40px">
            
            <p class="h3 text-secondary" style="margin: 15px"> Produtos em Geral Casdastrados </p>
            <hr>
            
            <table class="table table-striped">
              <thead class="">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col"><b>Nome</b></th>
                  <th scope="col"><b>Categoria</b></th>
                  <th scope="col"><b>Preço Normal</b></th>
                  <th scope="col"><b>Ação</b></th>
                </tr>
              </thead>
              <tbody id="tabelaGeral">
             
                
              </tbody>
            </table>
          </div>
        </div>
       
      </main>
    </div>
    <script>
      //preenche as tabelas ao 
      $(document).ready(function(){
        popularTabelaPromocao(1);
        popularTabelaGeral(0);
      });

      //SALVAR FORMULARIO
      $('#form').submit(function(e){
        e.preventDefault();

        var formulario = $(this);
      })



      //função para popular a tabela de produtos em PROMOÇÃO
      function popularTabelaPromocao(promo){
        $('#tabelaPromo').empty(); //limpa a tabela
        $.ajax({
          type:'GET',
          dataType:'json',
          url:'produtos_json.php?promo=' + promo

        }).done(function(data){
          for(var i =0; data.length>i; i++){
            var v = data[i].preco_normal;
            var v2 = data[i].preco_promocional;
            var vNormal = v.replace('.',',');
            var vPromo = v2.replace('.',',');

            var idCripto = "9651235485"+data[i].id_produto+"0654794713";
            //preenche a tabela
            $('#tabelaPromo').append('<tr><td><img height="42" width="42" src='+data[i].imagem +'>' +'</td><td>' +data[i].nome + '</td><td>' +data[i].nome_categoria + '</td><td>R$' +vNormal + '</td><td>R$'+ vPromo +'</td><td><a  href="cpanel_editar_produto.php?id='+idCripto+'"><button class="btn"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>  <a id="excluirPromo" href="" alt="'+data[i].id_produto+'"><button class="btn"><i class="fa fa-trash" aria-hidden="true"></button></a></td></tr>');
          }

           //evento de clique no botao de excluir
          $('#tabelaPromo tr td #excluirPromo').click(function(e){
            e.preventDefault();
            var id = $(this).attr("alt");
            var linha = $(this).parent().parent();
            exluirRegistro(id,linha);
          })

          //evento do botão editar 
          $('#tabelaPromo tr td #editarPromo').click(function(e){
            e.preventDefault();
            var id = $(this).attr("title");

            
            
          })


        }).fail(function(){
          alert('erro interno. Contate o Administrador.')

        });
      }//fim função



      //função para popular a tabela de produtos em geral
      function popularTabelaGeral(promo){
        $('#tabelaGeral').empty(); //limpa a tabela
        $.ajax({
          type:'GET',
          dataType:'json',
          url:'produtos_json.php?promo=' + promo

        }).done(function(data){
          for(var i =0; data.length>i; i++){
            var v = data[i].preco_normal;
            var vNormal = v.replace('.',',');

            var idCripto = "9651235485"+data[i].id_produto+"0654794713";
   
            //preenche a tabela
            $('#tabelaGeral').append('<tr><td><img height="42" width="42" src='+data[i].imagem +'>' +'</td><td>' +data[i].nome + '</td><td>' +data[i].nome_categoria + '</td><td>R$' +vNormal +'</td><td> <a  href="cpanel_editar_produto.php?id='+idCripto+'"><button class="btn"><i class="fa fa-pencil" aria-hidden="true"></i></button></a> <a id="excluirProd" href="" alt="'+data[i].id_produto+'"><button class="btn"><i class="fa fa-trash" aria-hidden="true"></button></a></td></tr>');
          }

          //evento de clique no botao de excluir
          $('#tabelaGeral tr td #excluirProd').click(function(e){
            e.preventDefault();
            var id = $(this).attr("alt");
            var linha = $(this).parent().parent();
            exluirRegistro(id,linha);
          })


        }).fail(function(){
          alert('erro interno. Contate o Administrador.')

        });
      }//fim função
      

      //FUNÇÃO DE EXCLUIR REGISTRO
      function exluirRegistro(cod, elemento){
        $.ajax({
          type:'POST',
          data:"id="+cod,
          url:'excluir_produto.php',
          asyn: true
        }).done(function(data){
          $sucesso = $.parseJSON(data)['sucesso'];
          $mensagem = $.parseJSON(data)['mensagem'];

          if($sucesso){
            elemento.fadeOut();

          }else{
            alert($mensagem);
          }

        }).fail(function(){
          alert('Erro interno. Contate o administrador');
        })
      }//fim função





    </script>







    <!-- jQuery Plugins -->
    <script src="_js/jquery.min.js"></script>
    <script src="_js/bootstrap.min.js"></script>
    <script src="_js/slick.min.js"></script>
    <script src="_js/nouislider.min.js"></script>
    <script src="_js/jquery.zoom.min.js"></script>
    <script src="_js/main.js"></script>
  </body>
  </html>
  